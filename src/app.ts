import 'module-alias/register';

import mongoose from 'mongoose';
import path from 'path';
import { RestClient } from './rest/rest_client';
import { ChangelogsRoute } from './routes/changelogs_route';
import { SpellsRoute } from './routes/spells_route';
import { url } from './secure';

// parseImg(Buffer.from(utils.getBase64File('./Ablenkung.png'), 'base64')).then(result => {
//     console.log(result);
// });

const port = Number(process.env.PORT || 3000);
const client = new RestClient(path.join(__dirname, '/public'), port);

client.add_route(new SpellsRoute());
client.add_route(new ChangelogsRoute());

// Connects to database
mongoose.connect(url, {

}, (err) => {
    console.log('Error:', err);
});

client.run();
