import express, { NextFunction, Request, RequestHandler, Response } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import * as http from 'http';
import { RouterPath } from './router_path';

class RestClient {
    private readonly app = express();

    private readonly redirect_trailing = (req: Request, res: Response, next: NextFunction) => {
        const test = /\?[^]*\//.test(req.url);
        if (req.url.substr(-1) === '/' && req.url.length > 1 && !test) {
            res.redirect(301, req.url.slice(0, -1));
        } else {
            next();
        }
    };

    /**
     *
     * @param path The path for the static saved files e.g. index.html
     * @param port The port which the server will listen to
     */
    constructor(
        readonly path: string,
        readonly port = 3000,
    ) {
        this.startup();
    }

    /**
     * This middleware sends index.html unless path is defined in paths
     * @param middleware
     * @param paths
     * @private
     */
    private unless(middleware: RequestHandler, ...paths: Array<RegExp>): any {
        return (req: Request, res: Response, next: NextFunction) => {
            const pathCheck = paths.some(p => {
                console.log(p);
                return p.test(req.path);
            });
            pathCheck ? next() : middleware(req, res, next);
        };
    }

    /**
     * Adds any amount of middlewares to be used after the default ones
     * @param middlewares
     */
    public add_middlewares(...middlewares: Array<RequestHandler>): void {
        middlewares.forEach(mw => this.app.use(mw));
    }

    /**
     * Adds any amount of routes
     * @param routes
     */
    public add_route(...routes: Array<RouterPath>): void {
        routes.forEach(route => {
            this.app.use(route.path, route.get_router());
        });
    }

    /**
     * Starts the client
     *
     * @private
     */
    private startup(): void {
        // http://localhost:9000
        const corsOptions = {
            // origin: '*',
            // optionsSuccessStatus: 200, // For legacy browser support
        };
        this.app.use(cors(corsOptions));

        this.add_middlewares(
            morgan('combined'),
            express.json(),
            this.redirect_trailing,
            express.static(this.path),
            // (req, res, next) => {
            //     console.log(req.headers);
            // },
        );

        // unless
        // this.app.use(this.unless(
        //       history(),
        // ...this.paths.map(url => url.path_regex).values()
        //  ));
    }

    /**
     * Starts the server
     *
     * @returns - an instance of the running server
     */
    public run(): http.Server {
        return this.app.listen(this.port, () => {
            console.log(`Up and running on port ${this.port}`);
        });
    }
}

export { RestClient };
