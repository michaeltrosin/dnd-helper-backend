import * as fs from 'fs';

const getBase64File = (filePath: string): string => {
  const contents = fs.readFileSync(filePath, { encoding: 'base64' });
  return (`${contents}`);
};

const utils = {
  getBase64File,
};

export default utils;
