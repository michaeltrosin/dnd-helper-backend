import { model, Model, Schema } from 'mongoose';
import { SpellDocument } from './spell_document';

type SpellModel = Model<SpellDocument>;

const spellScheme: Schema = new Schema({
    level: Number,
    source_book: String,
    classes: [String],

    name: {
        english: String,
        german: String,
    },

    school: String,
    ritual: Boolean,

    time_consumption: {
        value: Number,
        format: String,
    },

    range: {
        value: Number,
        format: String,
    },

    attributes: String,
    target: String,

    components: {
        verbal: Boolean,
        somatic: Boolean,
        material: String,
    },

    duration: {
        concentration: Boolean,
        format: String,
        value: Number,
        additional: String,
    },

    description: String,
    higher_levels: String,
});

const SpellObj: SpellModel = model<SpellDocument>('spells', spellScheme, 'spells');

export { SpellObj, SpellModel };
