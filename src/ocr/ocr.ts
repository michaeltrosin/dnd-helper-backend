import { createWorker, ImageLike } from 'tesseract.js';

const worker = createWorker({
  // logger: m => console.log(m),
  langPath: './deu.traineddata',
});

const parseImg = (image: ImageLike): Promise<string> => {
  return new Promise<string>(async (resolve) => {
    await worker.load();
    await worker.loadLanguage('deu');
    await worker.initialize('deu');
    const { data: { text } } = await worker.recognize(image);
    await worker.terminate();
    resolve(text);
  });
};

export default parseImg;
